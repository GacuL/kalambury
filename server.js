/**
 * Created by Piotrek-PC on 2016-04-11.
 */
var app = require('http').createServer(handler);
var io = require('socket.io').listen(app);
var static = require('node-static');
var fileServer = new static.Server('./public');
var fs = require('fs');
var gameState = false;
var dictionary;
var users = [];
var currentDrawer;
var myScore = 0;

var currentWord;

fs.readFile('dictionary.txt', function (err, data) {
    if (err) {
        return console.error(err);
    }
    dictionary = data.toString('utf-8').split('\r\n');

});


var listener = app.listen(5000, function () {
    console.log('Listening on port ' + listener.address().port);
});

function handler(request, response) {

    request.addListener('end', function () {
        fileServer.serve(request, response); // this will return the correct file
    });
    request.resume();
}

var line_history = [];

io.sockets.on('connection', function (socket) {

    console.log("Connection opened.");
    socket.on('new user', function (data, callback) {
        var nicknames = users.map(function (obj) {
            return obj.nick;
        });
        if (nicknames.indexOf(data) != -1) {
            console.log(users);
            callback(false);
        } else {
            callback(true);
            socket.nickname = data;
            users.push({id: socket.id, nick: socket.nickname, score: myScore});

            console.log("User " + socket.nickname + " joined your chat" + "ID:" + socket.id);

            if (gameState) {
                socket.emit('unlockButton');
                socket.emit('canvasIsClear');
            } else {
                socket.emit('canvasIsClear');
            }
            io.sockets.emit('users', users);
            socket.broadcast.emit('userJoined', {nick: socket.nickname});
            socket.emit('youAreJoined', {nick: socket.nickname});
        }
    });
    for (var i in line_history) {
        socket.emit('draw_line', {line: line_history[i]});
    }

    socket.on('draw_line', function (data) {
        line_history.push(data.line);
        io.emit('draw_line', {line: data.line});
    });

    socket.on("setColor", function (color) {
        io.emit("getColor", color);
    });


    socket.on('send message', function (data) {
        io.sockets.emit('new message', {msg: data, nick: socket.nickname});
        if (data == currentWord && gameState == true) {
            io.sockets.emit('gameEnded', {nick: socket.nickname});
            for (var i = 0; i < users.length; i++) {
                if (users[i].id == socket.id) {
                    users[i].score = users[i].score + 10;
                    io.sockets.emit('users', users);
                    gameState = false;
                }
            }
        }
    });

    socket.on('clearCanvas', function () {
        io.sockets.emit('canvasIsClear');
    });

    socket.on('gameIsReady', function (data) {
        gameState = true;
        currentDrawer = data;
        var randomLine = Math.floor(Math.random() * dictionary.length),
            line = dictionary[randomLine],
            word = line.split(',');
        currentWord = word[0];

        io.sockets.emit('gameStarted', {nick: currentDrawer});
        socket.emit('currentDrawer', currentWord);
        return currentDrawer;
    });
    socket.on('disconnect', function () {
        if (!socket.nickname) {
            return;
        }

        io.sockets.emit('userLeft', {nick: socket.nickname});
        if (currentDrawer != null) {
            io.sockets.emit('gameIsSurrender', currentDrawer);
            gameState = false;
        }
        console.log("Connection closed. ");


        for (var i = 0; i < users.length; i++) {

            if (users[i].id == socket.id) {
                users.splice(users.indexOf(socket.nickname), 1);
                console.log("User " + socket.nickname + socket.id + " left your channel");
            }
        }
        io.sockets.emit('users', users);
    });
});
