"use strict";
document.addEventListener("DOMContentLoaded", function (e) {
    var mouse = {
        click: false,
        move: false,
        pos: {x: e.clientX - this.offsetLeft, y: e.clientY - this.offsetTop},
        pos_prev: false
    };

    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');
    var width = 500;
    var height = 500;

    canvas.width = width;
    canvas.height = height;


    canvas.onmousedown = function () {
        mouse.click = true;
    };
    canvas.onmouseup = function () {
        mouse.click = false;
    };

    canvas.onmousemove = function (e) {

        mouse.pos.x = (e.clientX - this.offsetLeft) / width;
        mouse.pos.y = (e.clientY - this.offsetTop) / height;
        mouse.move = true;
    };

    socket.on('draw_line', function (data) {
        var line = data.line;
        context.beginPath();
        context.moveTo(line[0].x * width, line[0].y * height);
        context.lineTo(line[1].x * width, line[1].y * height);
        socket.on("getColor", function (data) {
            context.strokeStyle = data.color;
        });
        context.stroke();
    });

    //////////////////BUTTONS SECTION//////////////////////////

    var $blueColor = $('#Blue');
    var $redColor = $('#Red');
    var $greenColor = $('#Green');
    var $purpleColor = $('#Purple');
    var $orangeColor = $('#Orange');
    var $brownColor = $('#Brown');
    var $blackColor = $('#Black');
    var $pinkColor = $('#Pink');
    var $yellowColor = $('#Yellow');


    $redColor.click(function () {
        socket.emit("setColor", {color: "#FF0000"});
    });
    $blueColor.click(function () {
        socket.emit("setColor", {color: "#0000ff"});
    });
    $greenColor.click(function () {
        socket.emit("setColor", {color: "#008000"});
    });
    $yellowColor.click(function () {
        socket.emit("setColor", {color: "#FFFF00"});
    });
    $orangeColor.click(function () {
        socket.emit("setColor", {color: "#FFA500"});
    });
    $brownColor.click(function () {
        socket.emit("setColor", {color: "#A52A2A"});
    });
    $purpleColor.click(function () {
        socket.emit("setColor", {color: "#800080"});
    });
    $blackColor.click(function () {
        socket.emit("setColor", {color: "#000000"});
    });
    $pinkColor.click(function () {
        socket.emit("setColor", {color: "#FF1493"});
    });

    function mainLoop() {
        if (mouse.click && mouse.move && mouse.pos_prev) {
            socket.emit('draw_line', {line: [mouse.pos, mouse.pos_prev]});
            mouse.move = false;
        }
        mouse.pos_prev = {x: mouse.pos.x, y: mouse.pos.y};
        setTimeout(mainLoop, 25);
    }

    mainLoop();

    $('#clearcanvas').click(function () {
        context.clearRect(0, 0, canvas.width, canvas.height);
        socket.emit('clearCanvas');
    });

    socket.on('gameEnded', function () {
        context.clearRect(0, 0, canvas.width, canvas.height);
    });

    socket.on('canvasIsClear', function () {
        context.clearRect(0, 0, canvas.width, canvas.height);
    });

    socket.on('gameIsSurrender', function () {
        context.clearRect(0, 0, canvas.width, canvas.height);
    });
});
