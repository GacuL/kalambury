/**
 * Created by Piotrek-PC on 2016-04-11.
 */
$(document).ready(function () {
    var $nickBox = $('#nickname');
    var $nickForm = $('#setNickname');
    var $people = $('#people');
    var $chatContent = $('#chatcontent');
    var $chatInput = $('#chatinput');
    var $messageForm = $('#send-message');
    var $readyToDraw = $('#readytodraw');
    var $canvas = $('#canvas');
    var $clearChat = $('#clearchat');
    var $colorButtons = $('#colorButtons');
    var $main = $('#main');
    var $nickFrame = $('#nickFrame');

    $nickForm.submit(function (e) {
        e.preventDefault();
        givenNick = $nickBox.val();
        if (givenNick.trim() == '') {
            alert("Podaj nick");
            return false;
        } else {
            socket.emit('new user', givenNick, function (data) {
                if (data == true) {
                    $nickFrame.hide();
                    $main.show();
                    $('#clearcanvas').hide();
                    $colorButtons.hide();
                } else {
                    alert("this nickname is already used");
                }
            });
            $nickBox.val('');
        }
    });

    socket.on('users', function (users) {
        $people.text('');
        for (var i in users) {
            $people.append('<p>' + '<b>' + users[i].score + '</b>' + ' | ' + '<b><font color="green">' + users[i].nick + ' </span></p>');
        }
    });

    socket.on('userJoined', function (user) {
        $chatContent.append('<p>&raquo; ' + '<b>' + user.nick + '</b>' + ' joined.</p>');
        chatScrollDown();
    });

    socket.on('youAreJoined', function (user) {
       // $people.text('');
        $chatContent.append('<p>&raquo; You are logged as ' + '<b><font color="red">' + user.nick + '</b>' + '</p>');
        //$people.append('<p>' + '<b>' + user.score + '</b>' + ' | ' + '<b><font color="blue">' + user.nick + ' &hearts; </span></p>');
    });

    socket.on('userLeft', function (user) {
        $chatContent.append('<p>&raquo; ' + user.nick + ' left.</p>');
        chatScrollDown();
    });

    $messageForm.submit(function (e) {
        e.preventDefault();
        socket.emit('send message', $chatInput.val());
        $chatInput.val('');
    });

    socket.on('new message', function (data) {
        $chatContent.append('<b>' + data.nick  + '</b>' + ':' + data.msg + "<br/>");
        chatScrollDown();
    });

    function chatScrollDown() {
        $chatContent.scrollTop($chatContent[0].scrollHeight);
    }

    //===============Logic Section ========////

    $readyToDraw.click(function () {
        socket.emit('gameIsReady', givenNick);
    });

    socket.on('gameStarted', function (data) {
        $chatContent.append('<p>&raquo; ' + data.nick + ' started drawing!.</p>');
        $readyToDraw.prop('disabled', true);
    });

    socket.on('unlockButton', function(){
        $readyToDraw.prop('disabled', true);
    });

    socket.on('currentDrawer', function (currentWord) {
        $chatContent.append('<p>&raquo; You are drawing right now!.</p>');
        $chatContent.append('<p>' + currentWord + '</p>');
        $('#clearcanvas').show();
        $('#colorButtons').show();
        $canvas.css("pointer-events", "auto");
        $chatInput.prop('disabled', true);
    });

    socket.on('gameEnded', function (data) {
        $people.text('');
        $chatContent.append('<p>&raquo; ' + data.nick + ' has won this round!.</p>');
        $chatInput.prop('disabled', false);
        $readyToDraw.prop('disabled', false);
        $canvas.css("pointer-events", "none");
        $('#clearcanvas').hide();
        $('#colorButtons').hide();
    });

    socket.on('gameIsSurrender', function(currentDrawer){
        $readyToDraw.prop('disabled', false);
        $chatContent.append('<p>&raquo; ' + currentDrawer + ' has left the game!. Nobody wins.</p>');
    });

    $clearChat.click(function () {
        $chatContent.empty();
    });
});
